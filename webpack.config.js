var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: {
        main: [
            'babel-polyfill',
            './src/lessons14-.js'
        ]
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'public'),
        publicPath: '/public/'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                include: path.join(__dirname, 'src'),
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015', 'stage-0', 'react']
                }
            },
            {
                test: /\.scss/,
                include: path.join(__dirname, 'src'),
                loader: 'style!css!sass'
            }
        ]


    }
};